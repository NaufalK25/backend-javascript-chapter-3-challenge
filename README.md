# Backend JavaScript Chapter 3 Challenge

Challenge for Chapter 3 from Binar Academy - Backend JavaScript class

### Prerequisites

1. [Git](https://git-scm.com/downloads)
    ```
    git --version
    ```
2. [PostgreSQL](https://www.postgresql.org/download/)
    ```
    psql --version
    ```

### Question

![Challenge 3](challenge.png)

### Solution

1. [PostgreSQL Query](sql.txt)

2. ERD diagram

    ![ERD](erd.png)
